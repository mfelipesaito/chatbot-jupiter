import pandas as pd
from fuzzywuzzy import fuzz

def create_suggestion(theme):
    if len(theme)==1:
        suggestion = 'Você quer uma sugestão em ' + theme[0]
    elif len(theme)>1:
        suggestion = 'Você quer uma sugestão em ' + ', '.join(theme)
    else:
        suggestion = 'Sem tema passado'
    return suggestion

def filter_teacher(x, teacher):
    enabled = 1 if fuzz.partial_ratio(x, teacher) > 90 else 0
    return enabled

def teacher_restriction(suggestion, teacher):
    if len(teacher)>0:
        documents = pd.read_excel('base_materias_20201012.xlsx')
        documents = documents[documents['disciplina'].str.contains(suggestion,case= False)]        
        documents['enabled'] = documents['lista_professores'].apply(lambda x: filter_teacher(x, teacher))
        # o certo eh escolher a disciplina de maior score
        suggestion_filter = doc_filter[doc_filter['enabled']>0]['disciplina'].iloc[0].replace('[','').replace(']','').replace('\'','')
        
    else:
        suggestion_filter =  suggestion
    
    return suggestion_filter

def credits_restriction(suggestion, credit):
    if credit>0:
        documents = pd.read_excel('base_materias_20201012.xlsx')
        documents = documents[documents['disciplina'].str.contains(suggestion,case= False)]        
        # o certo eh escolher a disciplina de maior score
        doc_filter['num_creditos_aula'] = doc_filter['num_creditos_aula'].astype(int)
        suggestion_filter = doc_filter[doc_filter['num_creditos_aula']==credit]['disciplina'].iloc[0].replace('[','').replace(']','').replace('\'','')
        
    else:
        suggestion_filter =  suggestion
    
    return suggestion_filter
