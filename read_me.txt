How to webhook DialogFlow to python
1) cd ../chatbot-callisto (where the folder is located)
2) Open Command Prompt and Run: ngrok http 5000 
	(the default webhook.py goes to 5000,if not rerun ngrok with the apporpriate port)
3) Open another Command Prompt and run the following:
	cd env/Scripts
	activate
	cd ..\.. (to return to chatbot-callisto folder)
	webhook.py
4) Go to DialogFlow console 'https://dialogflow.cloud.google.com/#/editAgent/chatbot-callisto-ovyg/'
	a) Copy URL in the ngrok command line (something like  https://5ee980af25ec.ngrok.io, but it changes each time you run ngrok)
	b) Go to Fulfillment Page in DiagloFlow Console	
	c) Paste the copied URL in the URL section
	d) Save it
5) Done

------------------------------------------------------------------
The output JSON of the DialogFlow chatbot can be accessed through the weebhook.py code. 
The output comes as a JSON, and inside the ['queryResult'] we can access the 'queryText' and parameters.