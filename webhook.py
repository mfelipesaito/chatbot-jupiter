# /index.py
from flask import Flask, request, jsonify, render_template
import os
import dialogflow
import requests
import json
import pusher
import handler

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/webhook', methods=['POST'])

def webhook():
    print('Running.....!!!!')
    data = request.get_json(silent=True)
    theme = data['queryResult']['parameters']['tema']
    credit = data['queryResult']['parameters']['creditos']
    teacher = data['queryResult']['parameters']['professor']
    #hour = data['queryResult']['parameters']['horario']

    theme = [elem for elem in theme if len(elem)>=2]

    suggestion = handler.create_suggestion(theme)

    suggestion = handler.teacher_restriction(suggestion, theme)
    suggestion = handler.credits_restriction(suggestion, credit)

    if len(theme)==1:    
        reply = {
            "fulfillmentText": "Se você quer estudar sobre " + theme[0]+ " eu recomendo a disciplina " + suggestion,
        }
    elif len(theme)>1:
        theme_str = ', '.join(theme)
        reply = {
            "fulfillmentText": "Se você quer estudar sobre " + theme_str+ " eu recomendo a disciplina " + suggestion,
        }
    else:
        reply = {
            "fulfillmentText": "Nenhum tema foi passado" + suggestion,
        }
        
    return jsonify(reply)

# run Flask app
if __name__ == "__main__":
    app.run()
